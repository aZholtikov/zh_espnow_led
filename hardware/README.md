# Tested on

1. AP-FUTURE RGB led controller. Built on Tuya WiFi module WB3S (BK7231N chip). Replacement with ESP-12E. [Photo](http://git.zh.com.ru/alexey.zholtikov/zh_espnow_led/src/branch/esp8266/hardware/AP-FUTURE_RGB). A connect EN to VCC is required. A pull-down to GND of GPIO15 is required.

```text
    Using menuconfig:

    Led type            RGB
    Red GPIO number     04
    Green GPIO number   12
    Blue GPIO number    14

    Using MQTT:

    "3,255,255,4,12,14"
```

2. FLYIDEA RGBW light E27 15W (coming soon)
3. FLYIDEA RGBWW light E14 7W (coming soon)

MQTT configuration message should filled according to the template "X1,X2,X3,X4,X5,X6". Where:

```text
X1 - Led type. 1 for W, 2 for WW, 3 for RGB, 4 for RGBW, 5 for RGBWW.
X2 - First white GPIO number. 0 - 48 (according to the module used), 255 if not used.
X3 - Second white GPIO number. 0 - 48 (according to the module used), 255 if not used.
X4 - Red GPIO number. 0 - 48 (according to the module used), 255 if not used.
X5 - Green GPIO number. 0 - 48 (according to the module used), 255 if not used.
X6 - Blue GPIO number. 0 - 48 (according to the module used), 255 if not used.

Legend:
W - Cold white or Warm white or one another color.
WW - Cold white + Warm white.
RGB - Red + Green + Blue colors.
RGBW - Red + Green + Blue + Cold white or Warm white colors.
RGBWW - Red + Green + Blue + Cold white + Warm white colors.
```
